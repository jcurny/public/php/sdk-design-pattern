<?php
namespace Jcurny\Sdk\DesignPattern\Creational;

use stdClass;

trait Singleton
{
    /**
     * @var stdClass
     */
    private static $instance;

    /**
     * Singleton constructor.
     */
    private function __construct()
    {
        // Has to be private
    }

    /**
     * Singleton __clone.
     */
    private function __clone()
    {
    }

    /**
     * @return stdClass
     */
    public static function &getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }
}

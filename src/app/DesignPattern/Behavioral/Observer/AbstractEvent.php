<?php
namespace Jcurny\Sdk\DesignPattern\Behavioral\Observer;

abstract class AbstractEvent
{
    /**
     * @return string
     */
    public static function getEventKey(): string
    {
        return static::class;
    }
}

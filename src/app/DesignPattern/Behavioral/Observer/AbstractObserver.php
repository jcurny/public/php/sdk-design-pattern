<?php
namespace Jcurny\Sdk\DesignPattern\Behavioral\Observer;

abstract class AbstractObserver
{
    /**
     * @param AbstractEvent $event
     */
    abstract public function execute(AbstractEvent $event);
}

<?php
namespace Jcurny\Sdk\DesignPattern\Behavioral\Observer\Manager;

use Jcurny\Sdk\DesignPattern\Behavioral\Observer\AbstractEvent;
use Jcurny\Sdk\DesignPattern\Behavioral\Observer\AbstractObserver;

class EventManager
{
    /**
     * @var array observer list
     */
    private $observers = [];

    public function dispatch(AbstractEvent $event): bool
    {
        if (empty($this->observers[$event->getEventKey()])) {
            return false;
        }
        /** @var AbstractObserver $observer */
        foreach ($this->observers[$event->getEventKey()] as $observer) {
            $observer->execute($event);
        }

        return true;
    }

    /**
     * @param string           $eventKey
     * @param AbstractObserver $observer
     *
     * @return bool
     */
    public function register(string $eventKey, AbstractObserver $observer): bool
    {
        if (!\in_array($observer, $this->observers, true)) {
            $this->observers[$eventKey][] = $observer;

            return true;
        }

        return false;
    }
}
